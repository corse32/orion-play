import { combineReducers } from 'redux'
import {
    SELECT_CONTACT,
    REQUEST_CONTACTS, RECEIVE_CONTACTS, FILTER_CONTACTS, SORT_CONTACTS
} from './actions'

function contacts(state = {
    isFetching:         false,
    contacts:           [],
    filteredContacts: [],
    selectedContact:    null
}, action) {
    switch (action.type) {

        case REQUEST_CONTACTS:
            return Object.assign({}, state, {
                isFetching: true,
            })
        case RECEIVE_CONTACTS:
            return Object.assign({}, state, {
                isFetching: false,
                contacts: action.contacts,
                filteredContacts: action.contacts
            })
        case SELECT_CONTACT:
            return Object.assign({}, state, {
                selectedContact: action.id 
            })
        case FILTER_CONTACTS: 
            return Object.assign({}, state, {
                filteredContacts: state.contacts.filter(contact=>contact.name.toLowerCase().includes(action.input.toLowerCase()))
            })
        case SORT_CONTACTS: 
            return Object.assign({}, state, {
                filteredContacts: state.filteredContacts.sort((a, b)=>{
                    if (a[action.key].toString().toLowerCase() < b[action.key].toString().toLowerCase()) return -1
                    if (a[action.key].toString().toLowerCase() > b[action.key].toString().toLowerCase()) return 1
                    return 0
                })
            })
        default:
            return state
    }
}

const rootReducer = combineReducers({
    contacts
})

export default rootReducer
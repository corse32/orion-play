import React, { Component } from 'react'
import { selectContact }    from './actions'


export default function BusinessCard(props) {
    const { dispatch, contact } = props
    const {name, email, website, company } = contact
    return (
        <div className="business-card">
            <div className="name">{name}</div>
            <div className="company">{company.name}</div>
            <div className="email">{email}</div>
            <div className="website">{website}</div>
            <div onClick={()=>{dispatch(selectContact(null)) }} className="close">X</div>
        </div>
    )
}
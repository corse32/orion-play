import fetch from 'isomorphic-fetch'

export const REQUEST_CONTACTS = 'REQUEST_CONTACTS'
export const RECEIVE_CONTACTS = 'RECEIVE_CONTACTS'
export const SELECT_CONTACT = 'SELECT_CONTACT'
export const FILTER_CONTACTS = 'FILTER_CONTACTS'
export const SORT_CONTACTS = 'SORT_CONTACTS'

export function selectContact(id) {
  return {
    type: SELECT_CONTACT,
    id
  }
}

export function filterBy(input) {
    return {
        type: FILTER_CONTACTS,
        input
    }
}
export function sortBy(key) {
    return {
        type: SORT_CONTACTS,
        key
    }
}

function requestContacts() {
  return {
    type: REQUEST_CONTACTS,
  }
}

function receiveContacts(json) {
  return {
    type: RECEIVE_CONTACTS,
    contacts: json,
  }
}

export function fetchContacts() {
  return dispatch => {
    dispatch(requestContacts())
    return fetch(`https://jsonplaceholder.typicode.com/users`)
      .then(response => response.json())
      .then(json => dispatch(receiveContacts(json)))
  }
}

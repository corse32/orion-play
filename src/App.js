import React, { Component }                     from 'react'    
import { connect }                              from 'react-redux' 
import { selectContact, fetchContacts, filterBy, sortBy } from './actions'
import configureStore                           from './configureStore.js'
import logo                                     from './logo.svg'
import BusinessCard                             from './BusinessCard'
import                                               './App.css'

const store = configureStore()

const Head = function( props ) {
     const { contacts, handleSort } = props
     if ( !contacts[0] ) return <thead />
     const keys = Object.keys(contacts[0]);
     return (
         <thead>
             <tr>
            { keys.map((prop, i) => 
                <th onClick={ ()=>{ handleSort(prop) } } key={i}>{ prop }</th>
            )}
            </tr>
         </thead>
     )
}

const Contact = function (props) {
    const { onClick, contact } = props
    return (
        <tr className="contact-row" onClick={ ()=>{onClick(contact.id) }}>
            { Object.keys(contact).map( (prop, i ) => 
                <td key={i}> <Field contents={contact[prop]} /> </td>
            ) }
        </tr> 
    )
}

const Field = function (props) {
    const { contents } = props
    if ( typeof contents !== 'object') return <span>{contents}</span>
    else return (
        <table>
            <tbody>
        {
            Object.keys(contents).map( (prop, i ) => {
                if (typeof contents[prop] === 'object') return <tr key={i}><td><Field contents={contents[prop]} /></td></tr>
                else return (
                    <tr key={i}>
                      <td>{ prop } : </td>
                      <td>{ contents[prop] }</td>
                    </tr>
                )
            }
        )}
            </tbody>    
        </table>
    )
}

class App extends Component {
    constructor(props) {
        super(props)

        this.handleSelect = this.handleSelect.bind(this)
        this.handleSearch = this.handleSearch.bind(this)
        this.handleSort =  this.handleSort.bind(this)
        this.input = null
    }
    
    handleSelect(id) {
        const { dispatch } = this.props
        dispatch(selectContact(id))        
    }

    handleSearch() {
        const { dispatch } = this.props
        dispatch(filterBy(this.input.value))
    }

    handleSort(key) {
        const { dispatch } = this.props
        dispatch(sortBy(key))
    }

    componentDidMount() {
        const { dispatch } = this.props
        dispatch(fetchContacts())
    }
    render() {
        const { filteredContacts, contacts, dispatch, isFetching, selectedContact } = this.props
        return (
                <div className="App">
                    <div className="App-header">
                        <h2>Welcome to Josh's demo</h2>
                    </div>
                    { isFetching && 
                        <img src={logo} className="App-logo" alt="logo" />
                    }
                    <div className="list">
                        <input ref={(input) => { this.input = input }} placeholder="Search By Name" type="search" />
                        <button onClick={ this.handleSearch }>Search</button>
                        <table>
                            <Head contacts={ filteredContacts } handleSort={ this.handleSort } />
                            <tbody>
                                { filteredContacts.map((contact, key) => 
                                    <Contact key={ key } onClick={this.handleSelect} contact={ contact }/>
                                )}
                            </tbody>
                        </table>
                    </div>
                    {
                        selectedContact && 
                        <BusinessCard contact={ contacts.filter(contact => contact.id === selectedContact)[0]} dispatch={this.props.dispatch}/>
                    }
                        
                </div>
        )
    }
}

export default connect(state => state.contacts)(App)